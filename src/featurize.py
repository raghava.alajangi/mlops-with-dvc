import pandas as pd
import yaml

raw_data_path = "data/Iris.csv"
feat_data_path = "data/featurize/featurize.csv"

with open('params.yaml') as conf_file:
    config = yaml.safe_load(conf_file)

dataset = pd.read_csv(raw_data_path)

dataset['sepal_length_to_sepal_width'] = dataset['SepalLengthCm'] / dataset[
    'SepalWidthCm']
dataset['petal_length_to_petal_width'] = dataset['PetalLengthCm'] / dataset[
    'PetalWidthCm']
featured_dataset = dataset[[
    'SepalLengthCm', 'SepalWidthCm', 'PetalLengthCm', 'PetalWidthCm',
    'sepal_length_to_sepal_width', 'petal_length_to_petal_width',
    'Species'
]]

featured_dataset.to_csv(feat_data_path, index=False)
