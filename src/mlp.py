import pandas as pd
import pickle
from sklearn.neural_network import MLPClassifier
import yaml
# from mlem.api import save

train_data_path = "data/processed/Iris_train.csv"
model_path = 'models/mlp_model.pkl'

with open('params.yaml') as conf_file:
    config = yaml.safe_load(conf_file)

random_state = config['mlp']['random_state']
max_iter = config['mlp']['max_iter']

train = pd.read_csv(train_data_path)
X_train = train.drop(columns=['Species']).to_numpy()
y_train = train['Species'].to_numpy()

# Fit a model
model = MLPClassifier(random_state=random_state, max_iter=max_iter)
model.fit(X_train, y_train)

pickle.dump(model, open(model_path, 'wb'))

# save(model, model_path, sample_data=X_train)
