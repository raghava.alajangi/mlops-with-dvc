import json

import pandas as pd
import matplotlib.pyplot as plt
import pickle
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score, precision_score
from sklearn.metrics import ConfusionMatrixDisplay
import yaml
# from mlem.api import load

test_data_path = "data/processed/Iris_test.csv"
model_path = "models/mlp_model.pkl"
mlp_loss_metric = "reports/metrics/mlp_loss.csv"
mlp_conf_path = "reports/plots/mlp_cnf.png"
mlp_metrics_path = "reports/metrics/metrics.json"

with open("params.yaml") as conf_file:
    config = yaml.safe_load(conf_file)

# Get the test data
test = pd.read_csv(test_data_path)
X_test = test.drop(columns=["Species"]).to_numpy()
y_test = test["Species"].to_numpy()

mlp_model = pickle.load(open(model_path, 'rb'))

y_pred_mlp = mlp_model.predict(X_test)

mlp_acc = accuracy_score(y_test, y_pred_mlp)
mlp_prec = precision_score(y_test, y_pred_mlp, average="weighted")
mlp_rec = recall_score(y_test, y_pred_mlp, average="weighted")

with open(mlp_metrics_path, "w") as outfile:
    json.dump({
        "mlp_accuracy": mlp_acc,
        "mlp_precision": mlp_prec,
        "mlp_recall": mlp_rec,
    },
        outfile)

# Get the loss
loss = mlp_model.loss_curve_

pd.DataFrame(loss, columns=["loss"]).to_csv(mlp_loss_metric, index=False)

disp = ConfusionMatrixDisplay.from_estimator(
    mlp_model, X_test, y_test, normalize="true", cmap=plt.cm.Blues
)
plt.savefig(mlp_conf_path)
